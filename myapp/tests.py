from django.test import TestCase, LiveServerTestCase, RequestFactory
from django.test import Client
from django.urls import resolve
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from . import views
from django.contrib.auth import authenticate
import time

#from .views import search

from django.test import TestCase, Client

# Create your tests here.
class Story8UnitTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
        username='jacob', email='jacob@…', password='top_secret')

    def test_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_page_uses_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'login.html')

    def test_user(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        c = Client()
        logged_in = c.login(username='testuser', password='112345')

    def test_details(self):
        request = self.factory.get('/login/')
        request.user = self.user
        response = views.user_login(request)
        userlog = authenticate(request.user)
        self.assertEqual(response.status_code, 200)